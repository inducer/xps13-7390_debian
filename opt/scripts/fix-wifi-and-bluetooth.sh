#!/bin/sh

set -x

sudo cp /lib/firmware/intel/ibt-19-32-1.ddc /lib/firmware/intel/ibt-19-32-4.ddc
sudo cp /lib/firmware/intel/ibt-19-32-1.sfi /lib/firmware/intel/ibt-19-32-4.sfi

git clone https://chromium.googlesource.com/chromiumos/third_party/linux-firmware chromiumos-linux-firmware
cd chromiumos-linux-firmware
sudo cp iwlwifi-*  /usr/lib/firmware/
cd /usr/lib/firmware
sudo ln -s iwlwifi-Qu-c0-hr-b0-50.ucode iwlwifi-Qu-b0-hr-b0-50.ucode
